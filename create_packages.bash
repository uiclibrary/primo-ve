#/usr/bin/env bash

# TODO: convert this to a Gulp task.  Currently it requires a BASH shell.

mainPackage="01CARLI_UIC-CARLI_UIC"
packages=("01CARLI_UIC-CARLI_UIC" "01CARLI_UIC-CARLI_UIC_TEST")

projectFolder=$PWD

srcDir="$projectFolder/primo-explore/custom"
targetDir="$projectFolder/packages"

# make sure packages directory exists
mkdir -p $targetDir

# needs to happen in context of custom packages
cd $srcDir
for package in ${packages[@]}; do
  zipFile=$targetDir/$package.zip

  # remove any extra files that Primo doesn't know about, e.g. Mac directory and Sass files
  makeZip="zip -FSrq $zipFile $package -x **/*.DS_Store -x **/*.scss -x **/*.map"

  if [ $package == $mainPackage ]; then
    $makeZip
  else
    # To create alternative packages (e.g. "01CARLI_UIC-CARLI_UIC_TEST"), the folder internal to the zip file, for upload to Primo, must be named with the correct package name.  The only way I could figure out how to do it, was to duplicate the package, create the zip, then delete the duplicate.  A bit messy, but it works.  -Allan (aberry3@uic.edu)

    cp -r $mainPackage $package
    $makeZip
    rm -r $package
  fi
done