(function () {
  "use strict";

  var app = angular.module("viewCustom", ["angularLoad"]);

  ///////////////////////////
  // CUSTOMIZATION EXAMPLE //
  ///////////////////////////

  // app.component("myInstitutionComponent", {
  //   template: `<span style="margin-left: 40%;">Hello World</span>`,
  // });

  // app.component("prmSearchBarAfter", {
  //   bindings: { parentCtrl: `<` },
  //   template: `<my-institution-component></my-institution-component>`,
  // });

  ///////////////
  // MAIN MENU //
  ///////////////

  /**
   * Remove "target" attributes in Main Menu anchor links, forcing pages to load in current tab
   */
  function removeTargetAttrs() {
    function remove() {
      var menulinks = document.querySelectorAll("prm-main-menu a");
      if (menulinks) {
        menulinks.forEach((link) => {
          // kill all "target" attributes in the main menu
          link.removeAttribute("target");

          // remove now-false Aria label
          var attr = "aria-label";
          var match = ", opens in a new window";
          if (link.getAttribute(attr) && link.getAttribute(attr).match(match)) {
            link.setAttribute(attr, link.getAttribute(attr).replace(match, ""));
          }
        });
      }
    }
    remove();
    var observer = new MutationObserver(function (mutations, me) {
      remove();
    });
    observer.observe(document, {
      attributes: true,
      childList: true,
      subtree: true,
    });
  }

  /////////////////
  // FULL RECORD //
  /////////////////

  // #alma_other_members > div.layout-align-start-center.layout-row > h3 > span
  {
    /* <span translate="nui.brief.results.tabs.getit_other">Get it from other institutions</span> */
  }

  /**
   * Collapse "get it from other institutions" dropdown in full record
   * @param {object} app - an Angular app
   */
  function collapseGetItFromOtherInstitutions(app) {
    app.component("prmAlmaOtherMembersAfter", {
      bindings: {
        parentCtrl: "<",
      },
      controller: [
        function () {
          var ctrl = this;

          this.$onInit = function () {
            {
              ctrl.parentCtrl.isCollapsed = true;
            }
          };
        },
      ],
    });
  }

  removeTargetAttrs();
  collapseGetItFromOtherInstitutions(app);

  ////////////////////////
  // GOOGLE TAG MANAGER //
  ////////////////////////

  /**
   * Add GTM (i.e. Google Analytics) to all pages
   */
  function GTM() {
    const container_id = "GTM-WBSHPL4";

    // Head tag
    const head_comment_open = document.createComment("Google Tag Manager");
    const head_comment_close = document.createComment("End Google Tag Manager");
    const head_script_element = document.createElement("script");
    const head_script_text = document.createTextNode(`
      (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
      new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
      j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
      'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
      })(window,document,'script','dataLayer','${container_id}');`);
    head_script_element.appendChild(head_script_text);

    document.head.appendChild(head_comment_open);
    document.head.appendChild(head_script_element);
    document.head.appendChild(head_comment_close);
  }
  GTM();

  /////////////
  // LIBCHAT //
  /////////////

  /**
   * Add chat widget to all pages
   */
  function LibChat() {
    var chatID = "325a3faf25b8f6445806d60e21bcc210";
    var chat_element = document.createElement("div");
    var protocol = document.location.protocol ? "https://" : "http://";
    var chat_script = document.createElement("script");

    document.body.appendChild(chat_element);
    chat_element.setAttribute("id", `libchat_${chatID}`);
    chat_script.setAttribute("type", "text/javascript");
    chat_script.setAttribute("async", "true");
    chat_script.setAttribute(
      "src",
      `${protocol}v2.libanswers.com/load_chat.php?hash=${chatID}`
    );
    document.body.appendChild(chat_script);
  }
  LibChat();

  //////////////
  // STACKMAP //
  //////////////

  /**
   * Integrate StackMap into results
   * note: StackMap integration has been removed, as of 2022-09-20.  AJB
   */

  //////////////
  // BROWZINE //
  //////////////

  /**
   * Append Browzine card to Journals tab
   */
  function appendJournalsCard() {
    var new_card = document.createElement("md-card");

    new_card.className = "default-card";
    new_card.innerHTML = `
    <md-card-title>
      <md-card-title-text>
        <span class="md-headline">Browzine</span>
      </md-card-title-text>
    </md-card-title>
    <md-card-content>
      <p><a href="https://browzine.com/libraries/81">Browzine</a> lets you flip through and read UIC Library-subscribed Journals, equivalent to browsing through physical Library stacks.</p>
    </md-card-content>
    <md-card-footer layout="row" style="text-align: right">
      <a class="uic_button md-button md-accent md-raised" href="https://browzine.com/libraries/81">Visit Browzine</a></a>
    </md-card-footer>`;

    function placeCard() {
      const existing_card = document.querySelectorAll("md-card")[0];
      if (window.location.href.match("jsearch")) {
        if (existing_card) {
          existing_card.parentElement.append(new_card);
        }
      }
    }

    // initial appearance on direct load
    const observer = new MutationObserver(function (mutations, me) {
      const existing_card = document.querySelectorAll("md-card")[0];
      if (existing_card) {
        placeCard();
        me.disconnect(); // stop observing
      }
    });

    // subsequent appearances after navigation
    let loc = window.location.href;
    setInterval(function () {
      if (loc != window.location.href) {
        placeCard();
        loc = window.location.href;
      }
    }, 100); // check every 100ms

    observer.observe(document, {
      childList: true,
      subtree: true,
    });
  }

  /**
   * Integrate Primo into results
   * @param {object} app - an Angular app
   */
  function primoIntegration(app) {
    var BROWZINE_LIBRARYID = "81";
    var BROWZINE_APIKEY = "c0cf7e5e-ca95-4c7b-bfe8-e296dda8f01a";
    var UNPAYWALL_EMAIL_ADDRESS_KEY = "lib-web@uic.edu";

    // Load BrowZine Adapter
    window.browzine = {
      libraryId: BROWZINE_LIBRARYID,
      apiKey: BROWZINE_APIKEY,

      journalCoverImagesEnabled: true,

      journalBrowZineWebLinkTextEnabled: true,
      journalBrowZineWebLinkText: "View Journal Contents",

      articleBrowZineWebLinkTextEnabled: true,
      articleBrowZineWebLinkText: "View Issue Contents",

      articlePDFDownloadLinkEnabled: true,
      articlePDFDownloadLinkText: "Download PDF",

      articleLinkEnabled: true,
      articleLinkText: "Read Article",

      printRecordsIntegrationEnabled: true,

      unpaywallEmailAddressKey: UNPAYWALL_EMAIL_ADDRESS_KEY,

      articlePDFDownloadViaUnpaywallEnabled: true,
      articlePDFDownloadViaUnpaywallText: "Download PDF (via Unpaywall)",

      articleLinkViaUnpaywallEnabled: true,
      articleLinkViaUnpaywallText: "Read Article (via Unpaywall)",

      articleAcceptedManuscriptPDFViaUnpaywallEnabled: true,
      articleAcceptedManuscriptPDFViaUnpaywallText:
        "Download PDF (Accepted Manuscript via Unpaywall)",

      articleAcceptedManuscriptArticleLinkViaUnpaywallEnabled: true,
      articleAcceptedManuscriptArticleLinkViaUnpaywallText:
        "Read Article (Accepted Manuscript via Unpaywall)",
    };

    var w = document.createElement("script");
    w.type = "text/javascript";
    w.async = true;
    w.src =
      "https://s3.amazonaws.com/browzine-adapters/primo/browzine-primo-adapter.js";
    var b = document.body;
    b.appendChild(w);

    app.controller(
      "prmSearchResultAvailabilityLineAfterController",
      function ($scope) {
        window.browzine.primo.searchResult($scope);
      }
    );

    app.component("prmSearchResultAvailabilityLineAfter", {
      bindings: { parentCtrl: "<" },
      controller: "prmSearchResultAvailabilityLineAfterController",
    });
  }
  appendJournalsCard();
  primoIntegration(app);
})();
